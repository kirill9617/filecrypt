#ifndef HASH_H
#define HASH_H
#include <iostream>
#include <crypto++/hex.h>
using namespace std;
namespace Crypt{

template <class Type>
class Hash{
    Type hash;
    byte digest[ Type::DIGESTSIZE ];
    string message;
public:
    string ToString(){
            CryptoPP::HexEncoder encoder;
            string output;
            encoder.Attach( new CryptoPP::StringSink( output ) );
            encoder.Put( digest, sizeof(digest) );
            encoder.MessageEnd();
            return string("(")+ string(Type::StaticAlgorithmName()) +string(") ")+ output;
        }
        Hash Calculate(){
            hash.CalculateDigest( digest, (byte*) message.c_str(), message.length() );
            return *this;
        }
        Hash():message(""){}
        Hash(string msg):message(msg){

        }
        Hash SetMsg(string msg){
            for (int i=0;i<Type::DIGESTSIZE;++i){
                digest[i]=0;
            }
            message= msg;
            return *this;
        }
    };
}
#endif // HASH_H
