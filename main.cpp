#include <iostream>
#include <time.h>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <crypto++/md5.h>
#include <crypto++/sha.h>
//#include <crypto++/hex.h>
//#include <crypto++/pwdbased.h>
#include <boost/program_options.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/parsers.hpp>
#include "hash.h"
#include "pbkdf2.h"

using namespace std;
using namespace CryptoPP;
namespace po = boost::program_options;

const int salt_length=128;
const int derived_length=128;
enum op{
 encryption=0,
    decryption
};
int main(int argc, char** argv)
{
    string in,out,pass,cipher,mode,func;
    op work;
    bool debug;
    int correct_argc=7;
    //boost::program-options
    po::options_description desc("General options");
    desc.add_options()
            ("help,h", "Показать это сообщение")
            ("encrypt,e", "зашифровать файл")
            ("decrypt,d","расшифровать файл")
            ("input,i", po::value<std::string>(&in), "имя входного файла")
            ("output,o", po::value<std::string>(&out), "имя выходного файла")
            ("password,p", po::value<std::string>(&pass), "пароль")
            ("cipher,c", po::value<std::string>(&cipher), "тип шифра")
            ("mode,m", po::value<std::string>(&mode), "режим шифрования")
            ("function,f", po::value<std::string>(&func), "тип хеш-функции")
            ("debug,D","show debug data")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if(vm.count("debug")){
        debug=true;
        cout<<vm.count("input")<<vm.count("output")<<vm.count("password")<<vm.count("cipher")<<vm.count("mode")<<vm.count("function")<<endl;
        cout<<"input="<<in<<"\n";
        cout<<"output="<<out<<"\n";
        cout<<"pass="<<pass<<"\n";
        cout<<"cipher="<<cipher<<"\n";
        cout<<"mode="<<mode<<"\n";
        cout<<"func="<<func<<"\n";
    }
    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    if (!vm.size()){
        cout << desc << "\n";
        return 1;
    }else if (vm.size()<correct_argc+debug) {
        cout<<"Not enough arguments\n";
        cout << desc << "\n";
        return 1;
    }else if(vm.size()>correct_argc+debug){
        cout<<"too much args\n";
        cout << desc << "\n";
        return 1;
    }else if(vm.count("encrypt") && vm.count("decrypt")){
        cout<<"Encrypt and decrypt can`t be choosen simultaneously\n";
        cout << desc << "\n";
        return 1;
    }else if(!vm.count("encrypt") && !vm.count("decrypt")){
        cout<<"Please select encrypt or decrypt\n";
        cout << desc << "\n";
        return 1;
    }else{
        if(vm.count("input")&&vm.count("output")&&vm.count("password")&&vm.count("cipher")&&vm.count("mode")&&vm.count("function")){
            work=(op)vm.count("encrypt");
            cout<<work;
        }else{
            cout<<"impoosible input\n";
            cout << desc << "\n";
            return 1;
        }
    }
    //*/



//    srand(time(NULL));
//    string message ="HelloWorld";
//    string password="pass";
//    cout <<message <<" -> " <<Crypt::Hash<Weak1::MD5>(message).Calculate().ToString()<< endl;
//    cout <<message <<" -> " <<Crypt::Hash<SHA1>(message).Calculate().ToString()<< endl;
//    //PKCS5_PBKDF2_HMAC<Weak1::MD5> pbkdf2;
//    Crypt::Pbkdf2<Weak::MD5,128,128> pbkdf2(password);
//    //    pbkdf2.DeriveKey(derived,derived_length,0,(byte *)password.c_str(),password.length(),salt,salt_length,1000,.5);

//    cout << "derived = "<<pbkdf2.Derive(100,1).ToString()<<endl;
//    cout << "derived = "<<pbkdf2.Derive(100,1).ToString()<<endl;

    return 0;
}

