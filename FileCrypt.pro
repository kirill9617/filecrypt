TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

OTHER_FILES += \
    Task.md \
    Hash.txt

QMAKE_CXXFLAGS += -std=c++11
LIBS += -lboost_program_options

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libcrypto++

HEADERS += \
    hash.h \
    pbkdf2.h
