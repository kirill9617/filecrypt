#ifndef PBKDF2_H
#define PBKDF2_H
#include <crypto++/pwdbased.h>
#include <crypto++/hex.h>
using namespace std;
namespace Crypt{

template <class T,unsigned int keylength,unsigned int saltlength>
class Pbkdf2{
    CryptoPP::PKCS5_PBKDF2_HMAC<T> pbkdf2;
    byte derived[keylength];
    byte salt[saltlength];
    byte *password;
    unsigned int passlength;

public:
    string ToString(){
            CryptoPP::HexEncoder encoder;
            string output;
            encoder.Attach( new CryptoPP::StringSink( output ) );
            encoder.Put( derived, sizeof(derived) );
            encoder.MessageEnd();
            return output;
        }
        Pbkdf2 Derive(unsigned int iterations, double time){
            for(int i=0;i<saltlength;++i){
                salt[i]=rand();
            }
            pbkdf2.DeriveKey(derived,keylength,0,password,passlength,salt,saltlength,iterations,time);
            return *this;
        }
//        Pbkdf2(){
//            memset(derive,0,sizeof(derive));
//            memset(salt,0,sizeof(salt));
//        }
        Pbkdf2(string pass){
            memset(derived,0,sizeof(derived));
            passlength=pass.length();
            password=new byte[passlength];
            for(int i=0;i<passlength;++i){
                password[i]=pass[i];
            }
        }
    };
}
#endif // PBKDF2_H
